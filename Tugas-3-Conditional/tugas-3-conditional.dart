import 'dart:io';

void main() {
  //no1
  stdout.write('Do you want to install this application (Y/N): ');
  var answer = stdin.readLineSync()!;
  (answer.toUpperCase() == 'Y')
      ? print("Anda akan menginstall aplikasi dart")
      : print("Aborted");
  print('');


  //no2
  stdout.write('Input your name: ');
  var name = stdin.readLineSync()!;
  stdout.write('Input your role(Werewolf/Penyihir/Guard): ');
  var role = stdin.readLineSync()!;
  if (name == '') {
    print('Nama harus diisi!');
  } else {
    if (role == '') {
      print('Hello $name, Pilih peranmu untuk memulai game!');
    } else {
      print('Selamat datang di Dunia Werewolf, $name');
      if (role.toUpperCase() == 'PENYIHIR') {
        print(
            'Halo $role $name, kamu dapat melihat siapa yang menjadi werewolf!');
      } else if (role.toUpperCase() == 'GUARD') {
        print(
            'Halo $role $name, kamu akan membantu melindungi temanmu dari serangan werewolf.');
      } else if (role.toUpperCase() == 'WEREWOLF') {
        print('Halo $role $name, Kamu akan memakan mangsa setiap malam!');
      } else {
        print('Halo $name, pilih role yang tersedia');
        
      }
    }
  }
  print('');



  //no3
  stdout.write('Masukkan hari: ');
  var day = stdin.readLineSync()!;
  switch (day.toUpperCase()) {
    case 'SENIN':
      print(
          'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
      break;
    case 'SELASA':
      print(
          'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
      break;
    case 'RABU':
      print(
          'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
      break;
    case 'KAMIS':
      print(
          'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
      break;
    case 'JUMAT':
      print('Hidup tak selamanya tentang pacar.');
      break;
    case 'SABTU':
      print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
      break;
    case 'MINGGU':
      print(
          'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
      break;
  }
  print('');

  
  //no 4
  var tanggal = 8;
  var bulan = 6;
  var tahun = 2001;
  var month;
  switch (bulan) {
    case 1:
      month = 'Januari';
      break;
    case 2:
      month = 'Februari';
      break;
    case 3:
      month = 'Maret';
      break;
    case 4:
      month = 'April';
      break;
    case 5:
      month = 'Mei';
      break;
    case 6:
      month = 'Juni';
      break;
    case 7:
      month = 'Juli';
      break;
    case 8:
      month = 'Agustus';
      break;
    case 9:
      month = 'September';
      break;
    case 10:
      month = 'Oktober';
      break;
    case 11:
      month = 'November';
      break;
    case 12:
      month = 'Desember';
      break;
  }
  print('$tanggal $month $tahun');
}
