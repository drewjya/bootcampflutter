//no1
teriak() {
  print('Halo Sanbers!');
}

//no2
kalikan(num1, num2) {
  return num1 * num2;
}

//no3
introduce(nama, age, address, hobby) {
  return "Nama saya $nama, umur saya $age tahun, alamat saya di $address, dan saya punya hobby yaitu $hobby!";
}

//no4
faktorial(number) {
  if (number <= 1) {
    return number;
  } else {
    return number*faktorial(number-1);
  }
}

void main() {
  var name = "Andre";
  var age = 19;
  var address = "Perumahan Bosowa, Makassar";
  var hobby = "Membaca";
  var result = kalikan(12, 4);
  var perkenalan = introduce(name, age, address, hobby);
  teriak();
  print(result);
  print(perkenalan);
  print(faktorial(6));
}
