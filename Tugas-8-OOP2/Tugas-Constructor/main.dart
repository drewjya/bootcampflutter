import 'employee.dart';

void main() {
  //orang1
  var orang1 = Employee.id("0023113");
  print("ID : ${orang1.id}");
  //orang2
  var orang2 = Employee.name("Andre Wijaya");
  print("Nama : ${orang2.name}");

  //orang3
  var orang3 = Employee.department("Defence");
  print("Dep : ${orang3.department}");
}
