import 'dart:io';
import 'armor_titan.dart';
import 'beast_titan.dart';
import 'human.dart';
import 'attack_titan.dart';

void main() {
  armor_titan armor = armor_titan();
  beast_titan beast = beast_titan();
  human man = human();
  attack_titan attack = attack_titan();
  stdout.write('Input Armor Titan Power: ');
  armor.powerPoint = (int.parse(stdin.readLineSync()!));
  stdout.write('Input Attack Titan Power: ');
  attack.powerPoint = (int.parse(stdin.readLineSync()!));
  stdout.write('Input Beast Titan Power: ');
  beast.powerPoint = (int.parse(stdin.readLineSync()!));
  stdout.write('Input Human Power: ');
  man.powerPoint = (int.parse(stdin.readLineSync()!));

  print("Armor Titan\nPower: " + armor.powerPoint.toString());
  print(armor.terjang());
  print("Attack Titan\nPower: " + attack.powerPoint.toString());
  print(attack.punch());
  print("Beast Titan\nPower: " + beast.powerPoint.toString());
  print(beast.lempar());
  print("Human Titan\nPower: " + man.powerPoint.toString());
  print(man.killAlltitan());
}
