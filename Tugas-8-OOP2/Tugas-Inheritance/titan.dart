class titan {
  int _powerPoint = 0;
  int get powerPoint => _powerPoint;
  set powerPoint(int value){
    if(value <0){
      value *= -1;
    }
    _powerPoint = value;
  }
}