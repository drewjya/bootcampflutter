import 'dart:io';
import 'bangun_datar.dart';
import 'persegi.dart';
import 'segitiga.dart';
import 'lingkaran.dart';

void main() {
  bangun_datar bangunDatar = bangun_datar();
  bangunDatar.luas();
  bangunDatar.keliling();
  print("Triangle");
  stdout.write('Input the base: ');
  double base = (double.parse(stdin.readLineSync()!));
  stdout.write('Input the height: ');
  double height = (double.parse(stdin.readLineSync()!));
  segitiga triangle = segitiga(base, height);
  print("Area : ${triangle.luas()}");
  print("Perimeter : ${triangle.keliling()}");
  print("");
  print("Square");
  stdout.write('Input the side: ');
  double panjang = (double.parse(stdin.readLineSync()!));
  persegi square = persegi(panjang);
  print("Area : ${square.luas()}");
  print("Perimeter : ${square.keliling()}");
  print("");
  print("Circle");
  stdout.write('Input the radius: ');
  double radius = (double.parse(stdin.readLineSync()!));
  lingkaran circle = lingkaran(radius);
  print("Area : ${circle.luas()}");
  print("Perimeter : ${circle.keliling()}");
}
