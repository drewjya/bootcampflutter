import 'bangun_datar.dart';

class lingkaran extends bangun_datar {
  double radius = 0;
  double _phi = 3.14;
  lingkaran(double radius) {
    this.radius = radius;
  }
  @override double luas() => radius * radius * _phi;
  @override double keliling() => radius * _phi * 2;
}
