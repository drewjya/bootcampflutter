import 'bangun_datar.dart';
import 'dart:math';

class segitiga extends bangun_datar {
  double alas = 0, tinggi = 0;
  segitiga(double alas, double tinggi) {
    this.tinggi = tinggi;
    this.alas = alas;
  }
  @override
  double luas() => alas * tinggi / 2;
  @override
  double keliling() => alas + tinggi + sqrt((alas * alas) + (tinggi * tinggi));
}
