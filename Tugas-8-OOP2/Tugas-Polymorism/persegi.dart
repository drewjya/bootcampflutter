import 'bangun_datar.dart';

class persegi extends bangun_datar {
  double panjang = 0;
  persegi (double panjang){
    this.panjang = panjang;
  }
  @override double luas() => panjang * panjang;
  @override double keliling() => panjang * 4;
}
