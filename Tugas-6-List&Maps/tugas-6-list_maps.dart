void main() {
  print(range(51, 29));
  print(rangeWithStep(100, 10, 10));
  var input = [
    ["0001", "Andre Wijaya", "Makassar", "08/06/2001", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "13/02/2000", "Melukis"],
    ["0003", "Winona", "Surabaya", "01/05/2002", "Menari"],
    ["0004", "Bitang Samudera", "Ambon", "08/06/2004", "Berkebun"],
  ];
  dataHandling(input);
  print(balikKata("Kasur"));
  print(balikKata("SanberCode"));
  print(balikKata("Haji"));
  print(balikKata("racecar"));
  print(balikKata("Sanbers"));
}

//no1
range(startNum, finishNum) {
  List<int> Mylist = [];
  if (startNum <= finishNum) {
    while (startNum <= finishNum) {
      Mylist.add(startNum);
      startNum++;
    }
    return Mylist;
  } else {
    while (finishNum <= startNum) {
      Mylist.add(finishNum);
      finishNum++;
    }
    return Mylist.reversed.toList();
  }
}

//no2
rangeWithStep(startNum, finishNum, step) {
  if (startNum <= finishNum) {
    var list = [for (var i = startNum; i <= finishNum; i += step) i];
    return list;
  } else {
    var list = [for (var i = startNum; i >= finishNum; i -= step) i];
    return list;
  }
}

//no3
dataHandling(List datas) {
  datas.forEach((data) {
    print("Nomor ID: ${data[0]}");
    print("Nama Lengkap: ${data[1]}");
    print("TTL: ${data[2]}, ${data[3]}");
    print("Hobi: ${data[4]}");
    print('');
  });
}

//no4
balikKata(word) {
  return word.split('').reversed.join('');
}
