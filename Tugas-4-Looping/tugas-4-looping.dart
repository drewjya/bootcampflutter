// import 'dart:io';

import 'dart:io';

void main() {
  //no1
  var number = 0;
  print('LOOPING PERTAMA');
  while (number < 20) {
    number += 2;
    print('$number - I love Coding');
  }
  print('LOOPING KEDUA');
  while (number > 0) {
    print('$number - I love Coding');
    number -= 2;
  }
  print('');
  //no2
  for (int i = 1; i <= 20; i++) {
    if ((i % 2) == 0) {
      print('$i - Berkualitas');
    } else {
      if (i % 3 == 0) {
        print('$i - I Love Coding');
      } else {
        print('$i - Santai');
      }
    }
  }
  print('');
  //no3
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 8; j++) {
      stdout.write('#');
    }
    print('');
  }
  print('');

//no4
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < i; j++) {
      stdout.write('#');
    }
    print('');
  }
}
