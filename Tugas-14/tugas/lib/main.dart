import 'package:flutter/material.dart';
import 'Tugas/Tugas14/Get_data.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GetDataApi(),
    );
  }
}
