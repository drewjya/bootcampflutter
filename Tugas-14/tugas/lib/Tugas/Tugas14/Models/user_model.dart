import 'dart:convert';

import 'package:flutter/foundation.dart';

class User {
  final String name, email, address;
  User(this.name, this.email, this.address);
}

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));
String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({this.name, this.email, this.address, this.id, this.createAt});
  String name, email, address, id;
  DateTime createAt;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        name: json["name"],
        email: json["email"],
        address: json["address"],
        id: json["id"],
        createAt: DateTime.parse(json["createAt"]),
      );
  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "address": address,
        "id": id,
        "createAt": createAt.toIso8601String(),
      };
}
