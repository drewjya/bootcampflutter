import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() => LoginScreen();

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 60,
              ),
              Center(
                child: Text.rich(
                  TextSpan(
                    text: 'Sanber Flutter',
                    style: TextStyle(
                      color: Color.fromRGBO(84, 197, 248, 1),
                      fontWeight: FontWeight.w600,
                      fontSize: 30,
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Image.asset(
                    'assets/img/logo.png',
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  labelText: 'Username',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  labelText: 'Password',
                  labelStyle: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ),
              ),
              SizedBox(
                height: 21,
              ),
              Center(
                child: Text(
                  'Forget Password',
                  style: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(41, 182, 246, 1),
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Center(
                child: Container(
                  height: 42,
                  width: 400,
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(84, 197, 248, 1),
                  ),
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'Login',
                      style: GoogleFonts.poppins(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 19,
              ),
              Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(
                  'Does not have account?',
                  style: GoogleFonts.poppins(
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                      fontSize: 16),
                ),
                Spacer(),
                Text(
                  'Sign In',
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.normal,
                    color: Color.fromRGBO(1, 87, 155, 1),
                    fontSize: 16,
                  ),
                ),
              ]),
            ],
          )),
    );
  }
}


