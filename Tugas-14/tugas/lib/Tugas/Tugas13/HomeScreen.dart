import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 60),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(icon: Icon(Icons.notifications), onPressed: () {}),
                  IconButton(
                      icon: Icon(Icons.add_shopping_cart_outlined),
                      onPressed: () {}),
                ],
              ),
              SizedBox(
                height: 37,
              ),
              Text.rich(
                TextSpan(
                    text: 'Welcome, ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(84, 197, 248, 1),
                    ),
                    children: [
                      TextSpan(
                        text: 'Andre',
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Color.fromRGBO(1, 87, 155, 1)),
                      ),
                    ]),
                style: TextStyle(fontSize: 50),
              ),
              SizedBox(
                height: 40,
              ),
              TextField(
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.search, size: 20),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  hintText: 'Search',
                ),
              ),
              SizedBox(
                height: 80,
              ),
              Text(
                'Recomended Places',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 300,
                child: GridView.count(
                  padding: EdgeInsets.zero,
                  crossAxisCount: 2,
                  childAspectRatio: 1.491,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    for (var city in cities) Image.asset('assets/img/$city.png')
                  ],
                ),
              )
            ],
          )),
    );
  }
}

final cities = ['Monas', 'Roma', 'Berlin', 'Tokyo'];
