import 'dart:io';
void main() {
  Segitiga triangle;
  triangle = Segitiga();
  triangle.setengah = 0.5;
  triangle.alas = double.parse(stdin.readLineSync()!);
  triangle.tinggi = double.parse(stdin.readLineSync()!);
  print(triangle.hitungLuas());
}

class Segitiga {
  double setengah = 0;
  double alas = 0;
  double tinggi = 0;
  double hitungLuas() {
    return setengah * tinggi * alas;
  }
}
