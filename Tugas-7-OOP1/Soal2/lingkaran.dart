class Lingkaran {
  double _phi = 3.14159265; 
  double _radius = 0;

  void set Radius(double value) {
    if(value < 0){
      value *=-1;
    }
    _radius = value;
  }
  double get hitungLuas => _phi  * _radius * _radius;
}