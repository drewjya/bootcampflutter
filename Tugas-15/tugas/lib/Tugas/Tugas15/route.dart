import 'package:flutter/material.dart';
import 'package:tugas/Tugas/Tugas13/LoginScreen.dart';
import 'package:tugas/Tugas/Tugas13/HomeScreen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/home':
        return MaterialPageRoute(builder: (_) => HomeScreen());
        break;
      case '/about':
        // return MaterialPageRoute(builder: (_) => );
        break;
      case 'search':
        // return MaterialPageRoute(builder: (_)=>);
        break;
      default:
        return _errorRoute();
    }
  }


static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text("Error")),
        body: Center(child: Text('Error page')),
      );
    });
  }
}