import 'dart:async';

class Human {
  String name = "Nama character one piece";
  void getData() {
    name = "Andre";
    var timer = Timer(Duration(seconds: 3),
      () => print("Get Data [done]"));
  }
}

void main() async {
  var h = Human();
  print("Luffy");
  print("Zoro");
  print("Killer");
  h.getData();
  print('New Member : ${h.name}');
}
